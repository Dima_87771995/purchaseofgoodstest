import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import models.WildberriesOperations;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import wildberries_tests.BaseTest;

import static java.util.Arrays.asList;

@DisplayName("Wildberries cart test")
public class WildberriesCartCleanSeleniumTest extends BaseTest {

    private final String product = "//div[@class='product-card__brand-name']//strong[text() = '%s ']";
    private final String size = "//label[@class='j-size']//span[text()='%d']";
    private final String brand = "//div[@class='list-item__wrap']//span[text()='%s']//..//..//..//..//div[contains(@class,'btn__del')]";

    WildberriesOperations wildberriesOperations = new WildberriesOperations();

    @Before
    public void precondition() {
        driver.get("https://www.wildberries.ru/");

        wildberriesOperations
                .setMainTitle("Wildberries – Интернет-магазин модной одежды и обуви")
                .setDeliveryMessage("Бесплатная доставка")
                .setProductName("джинсы")
                .setFoundMessage("По запросу «джинсы» найдено")
                .setBrandName("Zamira")
                .setSize(29)
                .setCartIsEmptyMessage("В корзине пока ничего нет\n" +
                                       "Начните с главной страницы или воспользуйтесь поиском, чтобы найти что-то конкретное\n" +
                                       "ПЕРЕЙТИ НА ГЛАВНУЮ")
                .setData(asList("sdfs", "asfsf"));
    }

    @Epic("TC-56232")
    @Story("work with cart")
    @Test
    public void testBuyGoods() {

        String title = driver.getTitle();
        Assert.assertTrue(title.equals(wildberriesOperations.getMainTitle()));

        WebElement mainHeader = driver.findElement(By.cssSelector("a.simple-menu__link"));
        wait.until(ExpectedConditions.textToBePresentInElement(mainHeader,wildberriesOperations.getDeliveryMessage()));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.nav-element__logo")));

        WebElement header = driver.findElement(By.cssSelector("input.search-catalog__input.j-search-input"));
        header.sendKeys(wildberriesOperations.getProductName());

        WebElement searchButton = driver.findElement(By.cssSelector("button.search-catalog__btn.search-catalog__btn--search"));
        searchButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.nav-element__logo")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h1.searching-results__title")));

        WebElement foundResult = driver.findElement(By.cssSelector("h1.searching-results__title"));
        Assert.assertEquals(wildberriesOperations.getFoundMessage(), foundResult.getText());

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='product-card__brand-name']")));

        WebElement productByName = driver.findElement(By.xpath(String.format(product, wildberriesOperations.getBrandName()))); // добавить String name
        productByName.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.nav-element__logo")));

//        WebElement productName = driver.findElement(By.xpath("//h1[@class='same-part-kt__header']//span[text()='Zamira']")); // добавить %s
//        wait.until(ExpectedConditions.textToBePresentInElement(productName, wildberriesOperations.getBrandName()));
//
//        WebElement selectSize = driver.findElement(By.xpath(String.format(size, wildberriesOperations.getSize())));
//        selectSize.click();
//
//        WebElement addToCart = driver.findElement(By.cssSelector("button.btn-main"));
//        addToCart.click();
//
//        WebElement goToCart = driver.findElement(By.cssSelector("a[href='/lk/basket']"));
//        goToCart.click();
//
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h1.basket-section__header")));
//
//        WebElement deleteFromCart = driver.findElement(By.xpath(String.format(brand, wildberriesOperations.getBrandName())));
//        Dimension sizeElem = deleteFromCart.getSize();
//        actions.moveToElement(deleteFromCart, sizeElem.getWidth() - 1, sizeElem.getHeight() - 1).click().build().perform();
//        deleteFromCart.click();
//
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.basket-page__basket-empty.basket-empty")));
//
//        WebElement cartIsEmpty = driver.findElement(By.cssSelector("div.basket-page__basket-empty.basket-empty"));
//        Assert.assertEquals(wildberriesOperations.getCartIsEmptyMessage(), cartIsEmpty.getText());

    }
}
