package models;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class WildberriesOperations {

    private String mainTitle;
    private String productName;
    private String brandName;
    private String foundMessage;
    private Integer size;
    private String cartIsEmptyMessage;
    private String deliveryMessage;
    List<String> data;
}
